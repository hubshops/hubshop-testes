package br.com.hubshop.suites.syncmagento;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.com.hubshop.framework.BasePO;

public class SyncMagentoPO extends BasePO{

	@FindBy(how=How.ID,id="syncMagento:syncPanel:tabInstallModule")
	private static WebElement howToInstallModuleText;
	
	@FindBy(how=How.XPATH,xpath="//div[@id='syncMagento:syncPanel']/h3")
	private static WebElement howToInstallModulePanel;
	
	@FindBy(how=How.XPATH,xpath="//div[@id='syncMagento:syncPanel']/h3[2]")
	private static WebElement syncMagentoPanel;
	
	@FindBy(how=How.XPATH,xpath="//div[@id='syncMagento:syncPanel:syncProducts']/div[2]/span")
	private static WebElement syncProductsCheckbox;
	
	@FindBy(how=How.XPATH,xpath="//div[@id='syncMagento:syncPanel:syncAttributes']/div[2]/span")
	private static WebElement syncAttributesCheckbox;
	
	@FindBy(how=How.XPATH,xpath="//div[@id='syncMagento:syncPanel:syncCategories']/div[2]/span")
	private static WebElement syncCategoriesCheckbox;
	
	@FindBy(how=How.ID,id="syncMagento:syncPanel:syncButton")
	private static WebElement syncSubmitButton;
	
	private static SyncMagentoPO syncMagentoPO;
	
	private static String progressBarId = "importingProductsProgressBar";
	
	private static SyncMagentoPO getSyncMagentoPOInstance(){
		if(syncMagentoPO == null){
			syncMagentoPO = new SyncMagentoPO();
		}
		return syncMagentoPO;
	}
	
	public static SyncMagentoPO goToPage(){
		PageFactory.initElements(getDriverInstance(),SyncMagentoPO.class);
		goToUrl(systemUrl+"channel/syncMagentoChannel.xhtml");
		return getSyncMagentoPOInstance();
	}
	
	public static Boolean isInstallModulePanelOpen(){
		return isPanelOpen(howToInstallModulePanel);
	}
	
	public static Boolean isSyncMagentoPanelOpen(){
		return isPanelOpen(syncMagentoPanel);
	}
	
	public static String getHowToInstallModuleText(){
		return getElementText(howToInstallModuleText);
	}	
		
	public static SyncMagentoPO sync(){
		if(!isPanelOpen(syncMagentoPanel)){
			openPanel(syncMagentoPanel);
		}
		return getSyncMagentoPOInstance();
	}
	
	public SyncMagentoPO only(){
		unmarkCheckbox(syncAttributesCheckbox);
		unmarkCheckbox(syncCategoriesCheckbox);
		unmarkCheckbox(syncProductsCheckbox);
		return getSyncMagentoPOInstance();
	}
	
	public void all(){
		markCheckbox(syncAttributesCheckbox);
		markCheckbox(syncCategoriesCheckbox);
		markCheckbox(syncProductsCheckbox);
		clickAndWaitLoadingMessageAt(syncSubmitButton);
		checkIsProgressBarShow();
	}
	
	public static void checkIsProgressBarShow(){
		findElement(withId(progressBarId));
	}
	
	public void categories(){
		markCheckbox(syncCategoriesCheckbox);
		clickAndWaitLoadingMessageAt(syncSubmitButton);
	}
	
	public void products(){
		markCheckbox(syncProductsCheckbox);
		clickAndWaitLoadingMessageAt(syncSubmitButton);
		checkIsProgressBarShow();
	}

	public void attributes(){
		markCheckbox(syncAttributesCheckbox);
		clickAndWaitLoadingMessageAt(syncSubmitButton);
	}
	
	

public static void showSynchronizeTab() {
		clickAt(syncMagentoPanel);
	}
	
	public static void unmarkProductCheckbox() {
		clickAt(syncProductsCheckbox);
	}
	
	public static void unmarkAttributeCheckbox() {
		clickAt(syncAttributesCheckbox);
	}
	
	public static void unmarkCategoryCheckbox() {
		clickAt(syncCategoriesCheckbox);
	}
	
	public static void synchronize() {
		clickAt(syncSubmitButton);
	}
	
	public static boolean getResultMessage(String expected) {		
		WebDriverWait wait = new WebDriverWait(getDriverInstance(), 60);
		//return wait.until(ExpectedConditions.textToBePresentInElement(findElementByXpath("//*[@id=\"j_idt41:messages_container\"]/div/div/div[2]/span"), expected));
		return wait.until(ExpectedConditions.textToBePresentInElement(findElement(withClassName("ui-growl-title")), expected));
	}
	
	public static String getSyncTabDisplay() {
		WebElement we = findElement(withXpath("/html/body/div[3]/div[2]/div/div/div/div/form/div/div[2]"));
		return we.getCssValue("display");
	}
	
	public static void markOnlyAttributes() {
		unmarkProductCheckbox();
		unmarkCategoryCheckbox();
	}
	
	public static void markOnlyProducts() {
		unmarkAttributeCheckbox();
		unmarkCategoryCheckbox();
	}
}
