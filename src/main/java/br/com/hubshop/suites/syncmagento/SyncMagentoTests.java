package br.com.hubshop.suites.syncmagento;

import org.testng.annotations.Test;

import br.com.hubshop.suites.HubshopTest;

public class SyncMagentoTests extends HubshopTest{

	
	@Test
	public void checkFirstAcess(){
		SyncMagentoPO.goToPage();
		assertThat(SyncMagentoPO.isInstallModulePanelOpen()).isTrue();
		assertThat(SyncMagentoPO.getHowToInstallModuleText()).isEqualTo("Baixe o módulo blá blá blá");
	}
	
	@Test(dependsOnMethods="checkFirstAcess")
	public void importOnlyCategories(){
		SyncMagentoPO.sync().only().categories();
		assertThat(SyncMagentoPO.getAllFeedbackMsgs()).hasSize(2);
	}
	
	@Test(dependsOnMethods="importOnlyCategories")
	public void checkSecondPanelIsOpened() throws InterruptedException{
		SyncMagentoPO.goToPage();
		assertThat(SyncMagentoPO.isInstallModulePanelOpen()).isFalse();
		assertThat(SyncMagentoPO.isSyncMagentoPanelOpen()).isTrue();
	}
	
	@Test(dependsOnMethods="checkSecondPanelIsOpened")
	public void importOnlyAttributes(){
		SyncMagentoPO.sync().only().attributes();
		assertThat(SyncMagentoPO.getFeedbackMsg()).isEqualTo("A sincronização dos atributos está sendo realizada");
	}
	
	@Test(dependsOnMethods="importOnlyAttributes")
	public void importOnlyProducts() throws InterruptedException{
		SyncMagentoPO.sync().only().products();
		assertThat(SyncMagentoPO.getFeedbackMsg()).isEqualTo("A sincronização dos produtos está sendo realizada");
		Thread.sleep(3500);
		assertThat(SyncMagentoPO.getFeedbackMsg()).isEqualTo("Operação realizada com sucesso");
		
	}
	
	@Test(dependsOnMethods="importOnlyProducts")
	public void importAll() throws InterruptedException{
		SyncMagentoPO.sync().all();;
		Thread.sleep(3500);
		assertThat(SyncMagentoPO.getFeedbackMsg()).isEqualTo("Operação realizada com sucesso");
	}
}
