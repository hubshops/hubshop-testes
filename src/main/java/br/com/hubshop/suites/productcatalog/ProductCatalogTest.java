package br.com.hubshop.suites.productcatalog;

import org.testng.annotations.Test;

import br.com.hubshop.suites.HubshopTest;

public class ProductCatalogTest extends HubshopTest {
	
	@Test
	public void checkOrderingIsWorking(){
		ProductCatalogPO.goToPage();
		assertThat(ProductCatalogPO.getFirstProdutName()).isEqualTo("Abridor De Garrafa Que Toca Hino Do Botafogo");
		ProductCatalogPO.orderProductsOnlyByNameDesc();
		assertThat(ProductCatalogPO.getFirstProdutName()).isEqualTo("Abridor De Garrafa Que Toca Hino Do São Paulo");
		ProductCatalogPO.orderProductsOnlyByStockDesc();
		assertThat(ProductCatalogPO.getFirstProdutName()).isEqualTo("Abridor de garrafa que toca hino do Fluminense");
		//TODO: Falta por categoria
	}
	
	@Test(dependsOnMethods="checkOrderingIsWorking")
	public void checkFilterIsWorking(){
		ProductCatalogPO.filterProductsOnlyByName("São Paulo");
		assertThat(ProductCatalogPO.getFirstProdutName()).isEqualTo("Abridor De Garrafa Que Toca Hino Do São Paulo");
		ProductCatalogPO.filterProductsOnlyByStock("3");
		assertThat(ProductCatalogPO.getFirstProdutName()).isEqualTo("Abridor de garrafa que toca hino do Fluminense");
		ProductCatalogPO.filterProductsOnlyByCategory("Diversão");
		assertThat(ProductCatalogPO.getFirstProdutName()).isEqualTo("Abridor De Garrafa Que Toca Hino Do Botafogo");
		ProductCatalogPO.filterProductsOnlyByChannel("Nenhum marketplace");
		assertThat(ProductCatalogPO.isEmpty()).isTrue();
	}
	

}
