package br.com.hubshop.suites.productcatalog;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import br.com.hubshop.base.product.Product;
import br.com.hubshop.domain.ChannelEnum;
import br.com.hubshop.framework.BasePO;
import br.com.hubshop.framework.datatable.WebTable;
import br.com.hubshop.mercadolivre.listing.MercadoLivreListing;

public class ProductCatalogPO extends BasePO{

	private static final String DATATABLE_COMBO_LISTING_FILTER_ID = "producstForm:productsList:listingFilter";

	private static WebTable productsDatatable;
	
	private static ProductCatalogPO productCatalogPOInstance;
	private static MercadoLivreListing mlListingToConnect;
	private static List<MercadoLivreListing> listMlListingToConnect;
	private boolean isMultipleMlListing;
	
	@FindBy(how=How.ID,id="producstForm:productsList:nameColumn")
	private static WebElement nameColumn;
	
	@FindBy(how=How.ID,id="producstForm:productsList:stockColumn")
	private static WebElement stockColumn;
	
	@FindBy(how=How.ID,id="producstForm:productsList:nameColumn:filter")
	private static WebElement nameFilter;
	
	@FindBy(how=How.ID,id="producstForm:productsList:stockColumn:filter")
	private static WebElement stockFilter;
	
	@FindBy(how=How.ID,id="producstForm:productsList:categoriesColumn:filter")
	private static WebElement categoriesFilter;
	
	@FindBy(how=How.ID,id="producstForm:publishButton")
	private static WebElement publishButton;
	
	public static void selectAndPublishProduct(Product product){
		productsDatatable.findRowWithText(product.getName()).clickOnCheckBoxAtColumn(0);
		clickAt(publishButton);
	}
	
	
	public static void goToPage(){
		PageFactory.initElements(getDriverInstance(),ProductCatalogPO.class);
		goToUrl(systemUrl+"products/listProduct.xhtml");
		productsDatatable = findDataTableInsideOf(findElement(withXpath("//form[@id='producstForm']")));
	}
	
	private static ProductCatalogPO getInstance(){
		if(productCatalogPOInstance == null){
			productCatalogPOInstance = new ProductCatalogPO();
		}
		return productCatalogPOInstance;
	}
	
	public static ProductCatalogPO connect(MercadoLivreListing mlListing){
		mlListingToConnect = mlListing;
		getInstance().isMultipleMlListing = false;
		return getInstance();
	}
	
	public static void orderProductsOnlyByNameDesc(){
		clickAndWaitLoadingMessageAt(nameColumn);
		clickAndWaitLoadingMessageAt(nameColumn);
	}
	
	public static void orderProductsOnlyByStockDesc(){
		clickAndWaitLoadingMessageAt(stockColumn);
		clickAndWaitLoadingMessageAt(stockColumn);
	}
	
	public static void filterProductsOnlyByName(String filterName){
		typeText(filterName,nameFilter);
		waitForLoadingMessageDisapear();
		sleepDuring(2);
	}
	
	private static void clearAllFilters(){
		typeText("",nameFilter);
		typeText("",stockFilter);
		typeText("",categoriesFilter);
		//TODO: falta limpar filtro por channel
	}
	
	public static void filterProductsOnlyByStock(String stock){
		clearAllFilters();
		typeText(stock,stockFilter);
		waitForLoadingMessageDisapear();
		sleepDuring(2);
	}
	
	public static void filterProductsOnlyByCategory(String stock){
		clearAllFilters();
		typeText(stock,categoriesFilter);
		waitForLoadingMessageDisapear();
		sleepDuring(2);
	}
	
	public static void filterProductsOnlyByChannel(ChannelEnum channel){
		clearAllFilters();
		selectOptionByText(DATATABLE_COMBO_LISTING_FILTER_ID, channel.getLabel());
		waitForLoadingMessageDisapear();
		sleepDuring(2);
	}
	
	public static void filterProductsOnlyByChannel(String channel){
		clearAllFilters();
		selectOptionByText(DATATABLE_COMBO_LISTING_FILTER_ID,channel);
		waitForLoadingMessageDisapear();
		sleepDuring(2);
	}
	
	public static String getFirstProdutName(){
		return productsDatatable.getCellAt(2, 3).getText(); 
	}
	
	public static ProductCatalogPO connect(List<MercadoLivreListing> listMlListing){
		listMlListingToConnect = listMlListing;
		getInstance().isMultipleMlListing = true;
		return getInstance();
	}
	
	public void to(Product product){
		
		clickAndWaitLoadingMessageAt(
				productsDatatable.findRowWithText(product.getName())
				.getColumnAt(2));
		
		if(isMultipleMlListing){
			ProductDetailPO.connectListing(listMlListingToConnect);
		}
		else{
			ProductDetailPO.connectListing(mlListingToConnect);
		}
		
	}
	
	public static ProductCatalogPO disconnect(MercadoLivreListing mlListing){
		mlListingToConnect = mlListing;
		return getInstance();
	}	
	
	public void from(Product product){
		
		clickAndWaitLoadingMessageAt(
				productsDatatable.findRowWithText(product.getName())
					.getColumnAt(2));
		
		
		ProductDetailPO.disconnectListing(mlListingToConnect);
		
	}
	
	public static boolean isEmpty(){
		return productsDatatable.getFirstElement().getText().equals("Nenhum registro encontrado");
	}
	
	
	
}
