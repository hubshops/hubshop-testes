package br.com.hubshop.suites.productcatalog;

import java.util.List;

import br.com.hubshop.exception.TestFailedException;
import br.com.hubshop.framework.BasePO;
import br.com.hubshop.framework.datatable.WebTable;
import br.com.hubshop.mercadolivre.listing.MercadoLivreListing;

public class ProductDetailPO extends BasePO{
	
	private enum ProductDetailTabs{
		INFORMATION("Informações"), LISTINGS("Anúncios"), SYNC_HISTORY("Histórico de sincronizações");
		
		private String linkText;
		
		private ProductDetailTabs(String linkText){
			this.linkText = linkText;
		}
		
		public String getLinkText(){
			return this.linkText;
		}
		
	}

	
	public static void goToTab(ProductDetailTabs tab){
		switch (tab) {
		case LISTINGS:
			clickAndWaitLoadingMessageAt(findElement(withLinkText(tab.getLinkText())));
			break;

		default:
			break;
		}
	}
	
	public static void connectListing(MercadoLivreListing mlListing){
		goToTab(ProductDetailTabs.LISTINGS);
		clickOnConnectButton();
		connectListingFromPopup(mlListing);
		checkListingIsConnected(mlListing);
	}
	
	public static void connectListing(List<MercadoLivreListing> listMlListing){
		goToTab(ProductDetailTabs.LISTINGS);
		clickOnConnectButton();
		connectListingListFromPopup(listMlListing);
		checkListingListIsConnected(listMlListing);
	}
	
	private static void checkListingListIsConnected(List<MercadoLivreListing> listMlListing) {
		for(MercadoLivreListing mlListing : listMlListing){
			WebTable tableConnectedListings = findDataTableInsideOf(findElement(withId("productDetailsForm:tabsProductDetails:listAlreadyConnected")));
			tableConnectedListings.findRowWithText(mlListing.getName());			
		}
	}

	public static void disconnectListing(MercadoLivreListing mlListing){
		goToTab(ProductDetailTabs.LISTINGS);
		WebTable tableConnectedListings = findDataTableInsideOf(findElement(withId("productDetailsForm:tabsProductDetails:listAlreadyConnected")));
		tableConnectedListings.findRowWithText(mlListing.getName()).clickOnCheckBoxAtColumn(0);
		clickAndWaitLoadingMessageAt(findElement(withId("productDetailsForm:tabsProductDetails:disconnectProducts")));
		if(!feedbackMsgIsSuccessful()){
			throw new TestFailedException("Não foi possível desconectar produto");
		}
	}

	private static void checkListingIsConnected(MercadoLivreListing mlListing) {
		WebTable tableConnectedListings = findDataTableInsideOf(findElement(withId("productDetailsForm:tabsProductDetails:listAlreadyConnected")));
		tableConnectedListings.findRowWithText(mlListing.getName());
	}

	private static void connectListingFromPopup(MercadoLivreListing mlListing) {
		WebTable tableProductsNotConnected = findDataTableInsideOf(findElement(withId("productDetailsForm:tabsProductDetails:listNotConnected")));
		tableProductsNotConnected.findRowWithText(mlListing.getName()).clickOnCheckBoxAtColumn(0);
		clickAndWaitLoadingMessageAt(findElement(withId("productDetailsForm:tabsProductDetails:connectProductsDialod")));
	}
	
	private static void connectListingListFromPopup(List<MercadoLivreListing> listMlListing) {
		for(MercadoLivreListing mlListing : listMlListing){
			WebTable tableProductsNotConnected = findDataTableInsideOf(findElement(withId("productDetailsForm:tabsProductDetails:listNotConnected")));
			tableProductsNotConnected.findRowWithText(mlListing.getName()).clickOnCheckBoxAtColumn(0);
		}
		clickAndWaitLoadingMessageAt(findElement(withId("productDetailsForm:tabsProductDetails:connectProductsDialod")));			
	}

	private static void clickOnConnectButton() {
		clickAndWaitLoadingMessageAt(findElement(withId("productDetailsForm:tabsProductDetails:connectProducts")));
	}
	
}
