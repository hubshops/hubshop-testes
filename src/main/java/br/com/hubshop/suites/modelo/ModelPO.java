package br.com.hubshop.suites.modelo;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import br.com.hubshop.framework.BasePO;
import br.com.hubshop.suites.mercadolivre.mllistings.MlListingsPO;
import br.com.hubshop.suites.mercadolivre.sync.SyncMercadoLivrePO;

public class ModelPO extends BasePO {
	
	private static ModelPO modelPO;
	
	public static void goToPage(){
		PageFactory.initElements(getDriverInstance(),SyncMercadoLivrePO.class);
		goToUrl(systemUrl+"mercadoLivreListingModel/listMercadoLivreListingModel.xhtml");
	}
	
	private static ModelPO getModeloPOInstance(){
		if(modelPO == null){
			modelPO = new ModelPO();
		}
		return modelPO;
	}
	
	public static void test(){
		clickAndWaitLoadingMessageAt(findElement(withId("j_idt40:j_idt42")));
		clickAndWaitLoadingMessageAt(findElement(withId("j_idt75")));
	}
}
