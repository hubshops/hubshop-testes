package br.com.hubshop.suites.mercadolivre.site;

import br.com.hubshop.framework.BasePO;

public class MLHomePagePO extends BasePO{

	public static void goToPage(){
		goToUrl("http://www.mercadolivre.com");
	}
	
	public static void logoutFromCurrentLoggedAccount(){
		goToUrl("http://www.mercadolivre.com.br/jm/logout");
	}
	
	
}
