package br.com.hubshop.suites.mercadolivre.site;

import br.com.hubshop.mercadolivre.listing.MercadoLivreListing;

@SuppressWarnings("serial")
public class MercadoLivreListingAux extends MercadoLivreListing{

	private String listingDescription;
	private String priceOnPage;

	public String getListingDescription() {
		return listingDescription;
	}

	public void setListingDescription(String listingDescription) {
		this.listingDescription = listingDescription;
	}

	public String getPriceOnPage() {
		return priceOnPage;
	}

	public void setPriceOnPage(String priceOnPage) {
		this.priceOnPage = priceOnPage;
	}
}
