package br.com.hubshop.suites.mercadolivre.site;

import org.openqa.selenium.WebElement;

import br.com.hubshop.exception.TestFailedException;
import br.com.hubshop.framework.BasePO;

public class MLProductPagePO extends BasePO{

	
	private WebElement categoryName;
	
	private WebElement price;
	
	private WebElement title;
	
	private WebElement description;
	
	public MLProductPagePO checkIsEqualTo(MercadoLivreListingAux mlListing){
		if(!price.getText().equals(mlListing.getPriceOnPage())
		   || 	!title.getText().equals(mlListing.getName())
		   || 	!categoryName.getText().equals(mlListing.getCategory().getName())
		   || 	!description.getText().equals(mlListing.getListingDescription())
		   ){
			throw new TestFailedException("Valores do produto no mercado livre são diferentes do esperado");
		}
		return this;
	}
	
	private static MLProductPagePO init(){
		MLProductPagePO mLProductPagePO = new MLProductPagePO();
		mLProductPagePO.price = findElement(withXpath("//article/strong"));
		mLProductPagePO.title = findElement(withCssSelector("h1"));
		mLProductPagePO.categoryName = findElement(withCssSelector("div.nav-path-breadcrumb > ul"));
		mLProductPagePO.description = findElement(withCssSelector("#itemDescription > div > p"));
		return mLProductPagePO;
	}

	public static MLProductPagePO gotoProductPage(String productUrl) {
		goToUrl(productUrl);		
		return init();
	}
	
	public MLProductPagePO and(){
		return this;
	}
}
