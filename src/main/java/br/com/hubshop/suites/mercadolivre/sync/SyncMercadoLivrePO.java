package br.com.hubshop.suites.mercadolivre.sync;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import br.com.hubshop.framework.BasePO;
import br.com.hubshop.suites.mercadolivre.login.MlLoginPO;
import br.com.hubshop.suites.mercadolivre.login.MlUser;
import br.com.hubshop.suites.syncmagento.SyncMagentoPO;

public class SyncMercadoLivrePO extends BasePO{
	
	@FindBy(how=How.ID,id="syncMlForm:authorizeMl")
	private static WebElement authorizeFirstButton;
	
	@FindBy(how=How.ID,id="syncMlForm:authorizeOtherMl")
	private static WebElement authorizeOtherButton;
	
	
	public static void goToPage(){
		PageFactory.initElements(getDriverInstance(),SyncMercadoLivrePO.class);
		goToUrl(systemUrl+"channel/syncMercadoLivreChannel.xhtml");
	}
	
	public static void authorizeMLUsing(MlUser mlUser){
		try{
			clickAt(authorizeFirstButton);
		}
		catch(NoSuchElementException ex){
			clickAt(authorizeOtherButton);
		}
		MlLoginPO.loginAndAuthorizeUsing(mlUser);
		waitForElementApear(authorizeOtherButton);
	}
}
