package br.com.hubshop.suites.mercadolivre.login;

public class MlUser {

	private String user;
	private String passworkd;
	
	
	
	public MlUser(String user, String passworkd) {
		super();
		this.user = user;
		this.passworkd = passworkd;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassworkd() {
		return passworkd;
	}
	public void setPassworkd(String passworkd) {
		this.passworkd = passworkd;
	}
}
