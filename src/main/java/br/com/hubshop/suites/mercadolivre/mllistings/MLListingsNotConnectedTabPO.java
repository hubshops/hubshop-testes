package br.com.hubshop.suites.mercadolivre.mllistings;

public class MLListingsNotConnectedTabPO extends AbstractMLListingTab {

	
	private static MLListingsNotConnectedTabPO mLListingsNotConnectedTabPO;

	@Override
	public String getTabCss() {		
		return "#mlistingsForm\\:listingsTabs\\:notConnected";
	}

	@Override
	public String getTabType() {
		return "Não conectados";
	}
	
	public static AbstractMLListingTab getInstance() {
		if(mLListingsNotConnectedTabPO == null){
			mLListingsNotConnectedTabPO = new MLListingsNotConnectedTabPO();
		}
		return mLListingsNotConnectedTabPO;
	}

}
