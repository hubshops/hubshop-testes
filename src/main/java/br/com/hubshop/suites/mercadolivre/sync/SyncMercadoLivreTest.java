package br.com.hubshop.suites.mercadolivre.sync;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.Test;

import br.com.hubshop.base.product.Product;
import br.com.hubshop.mercadolivre.listing.MercadoLivreListing;
import br.com.hubshop.suites.HubshopTest;
import br.com.hubshop.suites.mercadolivre.login.MlUser;
import br.com.hubshop.suites.mercadolivre.mllistings.MlListingsPO;
import br.com.hubshop.suites.mercadolivre.mllistings.MlListingsPO.MlListingTabs;
import br.com.hubshop.suites.mercadolivre.site.MLHomePagePO;
import br.com.hubshop.suites.productcatalog.ProductCatalogPO;

public class SyncMercadoLivreTest extends HubshopTest {
    
	@Test
	public void  authorizeMLUserWithNoListings(){
		//SyncMercadoLivrePO.goToPage();
		//SyncMercadoLivrePO.authorizeMLUsing(new MlUser("TETE571793", "qatest5556"));
		//MlListingsPO.goToPage();
		//MlListingsPO.check(MlListingTabs.ALL).isEmpty();
	}
	
	@Test(dependsOnMethods="authorizeMLUserWithNoListings")
	public void  authorizeMLUserWithListings(){
		
		MLHomePagePO.logoutFromCurrentLoggedAccount();
		
		SyncMercadoLivrePO.goToPage();
		SyncMercadoLivrePO.authorizeMLUsing(new MlUser("TETE7225156", "qatest9735"));
		
		MercadoLivreListing mlBotafogo = new MercadoLivreListing();
		mlBotafogo.setName("Abridor De Garrafa Que Toca Hino Do Botafogo");
		
		MercadoLivreListing mlSaoPaulo = new MercadoLivreListing();
		mlSaoPaulo.setName("Abridor De Garrafa Que Toca Hino Do São Paulo");
		
		MercadoLivreListing mlAlgumHino = new MercadoLivreListing();
		mlAlgumHino.setName("Abridor De Garrafa Que Toca Algum Hino");
		
		MlListingsPO.goToPage();
		MlListingsPO.check(mlBotafogo).isPresentOn(MlListingTabs.ACTIVE);
		MlListingsPO.check(mlSaoPaulo).isPresentOn(MlListingTabs.ACTIVE);
		MlListingsPO.check(mlAlgumHino).isPresentOn(MlListingTabs.NOT_CONNECTED);
	}
	
	@Test(dependsOnMethods="authorizeMLUserWithListings")
	public void connectAProductToAListing(){
		ProductCatalogPO.goToPage();
		
		MercadoLivreListing mlFluminense = new MercadoLivreListing();
		mlFluminense.setName("Abridor De Garrafa Que Toca Algum Hino");
		
		Product productFluminense = new Product();
		productFluminense.setName("Abridor de garrafa que toca hino do Fluminense");
		
		ProductCatalogPO.connect(mlFluminense).to(productFluminense);
		
		MlListingsPO.goToPage();
		MlListingsPO.check(mlFluminense).isPresentOn(MlListingTabs.ACTIVE);
	}
	
	@Test(dependsOnMethods="connectAProductToAListing")
	public void disconnectAProductFromAListing(){
		
		ProductCatalogPO.goToPage();
		
		MercadoLivreListing mlFluminense = new MercadoLivreListing();
		mlFluminense.setName("Abridor De Garrafa Que Toca Algum Hino");
		
		Product productFluminense = new Product();
		productFluminense.setName("Abridor de garrafa que toca hino do Fluminense");
		
		ProductCatalogPO.disconnect(mlFluminense).from(productFluminense);
		
		
		MlListingsPO.goToPage();
		MlListingsPO.check(mlFluminense).isPresentOn(MlListingTabs.NOT_CONNECTED);
	}
	
	@Test(dependsOnMethods="disconnectAProductFromAListing")
	public void connectManyListingsToAProduct(){
		
		ProductCatalogPO.goToPage();
		
		MercadoLivreListing mlFluminense = new MercadoLivreListing();
		mlFluminense.setName("Abridor De Garrafa Que Toca Hino Do Fluminensee");
		MercadoLivreListing mlAlgumHino = new MercadoLivreListing();
		mlAlgumHino.setName("Abridor De Garrafa Que Toca Algum Hino");
		MercadoLivreListing mlAdaptador = new MercadoLivreListing();
		mlAdaptador.setName("Adaptador De Tomadas Universall");
		
		List<MercadoLivreListing> mlListingsToConnect = new ArrayList<MercadoLivreListing>();
		
		mlListingsToConnect.add(mlFluminense);
		mlListingsToConnect.add(mlAdaptador);
		mlListingsToConnect.add(mlAlgumHino);
		
		
		
		Product productFluminense = new Product();
		productFluminense.setName("Abridor de garrafa que toca hino do Fluminense");
		
		ProductCatalogPO.connect(mlListingsToConnect).to(productFluminense);
		
		MlListingsPO.goToPage();
		MlListingsPO.check(mlFluminense).isPresentOn(MlListingTabs.ACTIVE);
		MlListingsPO.check(mlAlgumHino).isPresentOn(MlListingTabs.ACTIVE);
		MlListingsPO.check(mlAdaptador).isPresentOn(MlListingTabs.ACTIVE);
	}
}
