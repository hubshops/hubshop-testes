package br.com.hubshop.suites.mercadolivre.mllistings;

public class MLListingsNotPublishedTabPO extends AbstractMLListingTab {
	
	private static MLListingsNotPublishedTabPO mLListingsNotPublishedTabPO;

	@Override
	public String getTabCss() {		
		return "#mlistingsForm\\:listingsTabs\\:notPublishedTab";
	}

	@Override
	public String getTabType() {
		return "Não publicados";
	}
	
	public static AbstractMLListingTab getInstance() {
		if(mLListingsNotPublishedTabPO == null){
			mLListingsNotPublishedTabPO = new MLListingsNotPublishedTabPO();
		}
		return mLListingsNotPublishedTabPO;
	}

}
