package br.com.hubshop.suites.mercadolivre.login;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import br.com.hubshop.framework.BasePO;
import br.com.hubshop.suites.mercadolivre.AuthorizeAppPO;

public class MlLoginPO  extends BasePO{
	
	@FindBy(how=How.ID,id="user_id")
	private static WebElement userInput;
	
	@FindBy(how=How.ID,id="password")
	private static WebElement passwordInput;
	
	@FindBy(how=How.ID,id="signInButton")
	private static WebElement signInButton;
	
	public static void loginAndAuthorizeUsing(MlUser mlUser){		
		PageFactory.initElements(getDriverInstance(),MlLoginPO.class);
		waitForElementApear(userInput);
		typeText(mlUser.getUser(), userInput);
		typeText(mlUser.getPassworkd(), passwordInput);
		clickAt(signInButton);
		AuthorizeAppPO.authorizeApp();
	}

}
