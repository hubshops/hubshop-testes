package br.com.hubshop.suites.mercadolivre.mllistings;

public class MLListingsActiveTabPO extends AbstractMLListingTab {
	
	private static MLListingsActiveTabPO mLListingsActiveTabPOInstance;
	
	
	public MLListingsActiveTabPO(){
		super();
	}
	
	@Override
	public String getTabCss() {
		return "#mlistingsForm\\:listingsTabs\\:activeTab";
	}

	@Override
	public String getTabType() {		
		return "Ativos";
	}

	public static AbstractMLListingTab getInstance() {
		if(mLListingsActiveTabPOInstance == null){
			mLListingsActiveTabPOInstance = new MLListingsActiveTabPO();
		}
		return mLListingsActiveTabPOInstance;
	}
}
