package br.com.hubshop.suites.mercadolivre.mllistings;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import br.com.hubshop.exception.TestFailedException;
import br.com.hubshop.framework.BasePO;
import br.com.hubshop.framework.datatable.WebTable;
import br.com.hubshop.mercadolivre.listing.MercadoLivreListing;

public abstract class AbstractMLListingTab extends BasePO{

	private  WebElement currentTab;
	
	private  WebTable currentDataTable;
	
	public abstract String getTabCss();
	
	public abstract String getTabType();
	
	public AbstractMLListingTab(){
		initElements();
	}
	
	public void initElements(){
		currentTab = findElement(withCssSelector(this.getTabCss()));
		currentDataTable = findDataTableInsideOf(currentTab);
	}

	public void isListingPresent(MercadoLivreListing mlListing){
		try{
			if(currentDataTable.findRowWithText(mlListing.getName()) != null){
				return;
			}
			throw new TestFailedException("Anúncio "+mlListing.getName()+"não localizado na aba "+getTabType());
		}
		catch(StaleElementReferenceException exc ){
			initElements();
			isListingPresent(mlListing);
		}
	}
	
	public void isEmpty(){
		if(!currentDataTable.getFirstElement().getText().equals("Nenhum registro encontrado")){
			throw new TestFailedException("Existem registros de anúncio ativos");
		}
	}
}
