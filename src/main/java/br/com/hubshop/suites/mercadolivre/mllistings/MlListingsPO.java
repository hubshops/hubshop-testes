package br.com.hubshop.suites.mercadolivre.mllistings;

import org.openqa.selenium.support.PageFactory;

import br.com.hubshop.framework.BasePO;
import br.com.hubshop.mercadolivre.listing.MercadoLivreListing;
import br.com.hubshop.suites.mercadolivre.sync.SyncMercadoLivrePO;

public class MlListingsPO extends BasePO {
	
	private static MlListingsPO mlListingsPO;
	
	private static MercadoLivreListing mlListingToCheckPresenceOf;
	private static MlListingTabs tabsToTest;
	
	public enum MlListingTabs{
		ACTIVE("Ativos"), NOT_CONNECTED("Não conectados"), NOT_PUBLISHED("Não publicado"), ALL("");
		
		private String linkText;
		
		private MlListingTabs(String linkText){
			this.setLinkText(linkText);
		}

		public String getLinkText() {
			return linkText;
		}

		public void setLinkText(String linkText) {
			this.linkText = linkText;
		}
	}
	
	
	private static MlListingsPO getMlListingsPOInstance(){
		if(mlListingsPO == null){
			mlListingsPO = new MlListingsPO();
		}
		return mlListingsPO;
	}
	
	@SuppressWarnings("incomplete-switch")
	public static void goToTab(MlListingTabs tab){
		switch (tab) {
		case ACTIVE:
			clickAndWaitLoadingMessageAt(findElement(withLinkText(MlListingTabs.ACTIVE.getLinkText())));
			break;
		case NOT_CONNECTED:
			clickAndWaitLoadingMessageAt(findElement(withLinkText(MlListingTabs.NOT_CONNECTED.getLinkText())));
			break;
		case NOT_PUBLISHED:
			clickAndWaitLoadingMessageAt(findElement(withLinkText(MlListingTabs.NOT_PUBLISHED.getLinkText())));
			break;
		}
	}
	
	public static void goToPage(){
		PageFactory.initElements(getDriverInstance(),SyncMercadoLivrePO.class);
		goToUrl(systemUrl+"mercadoLivreListing/listMercadoLivreListing.xhtml");
	}	
	
	public static MlListingsPO check(MercadoLivreListing mlListing){
		mlListingToCheckPresenceOf = mlListing;
		return getMlListingsPOInstance();
	}
	
	public static MlListingsPO check(MlListingTabs tab){
		tabsToTest = tab;
		return getMlListingsPOInstance();
	}
	
	@SuppressWarnings("incomplete-switch")
	public void isEmpty(){		
		switch (tabsToTest) {
			case ALL:
				
				goToTab(MlListingTabs.ACTIVE);
				MLListingsActiveTabPO.getInstance().isEmpty();
				
				goToTab(MlListingTabs.NOT_CONNECTED);
				MLListingsNotConnectedTabPO.getInstance().isEmpty();
				
				goToTab(MlListingTabs.NOT_PUBLISHED);
				MLListingsNotPublishedTabPO.getInstance().isEmpty();
				
				break;	
		}
	}
	
	@SuppressWarnings("incomplete-switch")
	public void isPresentOn(MlListingTabs tab){		
		switch (tab) {
			case ACTIVE:
				goToTab(MlListingTabs.ACTIVE);
				MLListingsActiveTabPO.getInstance().isListingPresent(mlListingToCheckPresenceOf);
				break;
			case NOT_CONNECTED:
				goToTab(MlListingTabs.NOT_CONNECTED);
				MLListingsNotConnectedTabPO.getInstance().isListingPresent(mlListingToCheckPresenceOf);
				break;
			case NOT_PUBLISHED:
				goToTab(MlListingTabs.NOT_PUBLISHED);
				MLListingsNotPublishedTabPO.getInstance().isListingPresent(mlListingToCheckPresenceOf);
				break;
			}
	}
}
