package br.com.hubshop.suites.mercadolivre;

import java.util.NoSuchElementException;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import br.com.hubshop.framework.BasePO;

public class AuthorizeAppPO extends BasePO{
	
	
	@FindBy(how=How.ID,id="authorize")
	private static WebElement authorizeButton;
	
	
	public static void authorizeApp(){
		PageFactory.initElements(getDriverInstance(),AuthorizeAppPO.class);
		try{
			waitForElementApear(authorizeButton,10);
			clickAt(authorizeButton);
		}
		catch(NoSuchElementException | TimeoutException ex){
			//TODO: Registrar como um log
			System.out.println("Não foi necessário autorizar");
		}
	}
	

}
