package br.com.hubshop.suites;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import br.com.hubshop.framework.BasePO;
import br.com.hubshop.framework.BaseTest;
import br.com.hubshop.suites.login.LoginPO;

public class HubshopTest extends BaseTest {
	
	@BeforeSuite
	public void login(){
//		LoginPO.login("admin", "admin");
	}
	
	@AfterSuite
	public void quit(){
		BasePO.closeBrowser();
	}
	
	public void reOpenBrowser(){
		BasePO.closeAndOpenBrowser();
		LoginPO.login("admin", "admin");
	}

}
