package br.com.hubshop.suites.login;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import br.com.hubshop.framework.BasePO;

public class LoginPO extends BasePO{
	
	@FindBy(how=How.ID,id="loginForm:login")
	private static WebElement loginInput;
	
	@FindBy(how=How.ID,id="loginForm:passwork")
	private static WebElement passwordInput;
	
	@FindBy(how=How.ID,id="loginForm:loginSubmit")
	private static WebElement loginButton;
	
	static{
		PageFactory.initElements(getDriverInstance(),LoginPO.class);		
	}
	
	public static void login(String login, String password){
		goToUrl(systemUrl+"login.xhtml");
		typeText(login,loginInput);
		typeText(password,passwordInput);
		clickAt(loginButton);
		findElement(withId("logo"));
	}
}
