package br.com.hubshop.suites.publish;

import java.util.concurrent.TimeUnit;

import br.com.hubshop.base.product.Product;
import br.com.hubshop.exception.TestFailedException;
import br.com.hubshop.framework.BasePO;
import br.com.hubshop.framework.datatable.WebTable;

public class PublishSummaryPO extends BasePO{
	
	private static WebTable summaryTable; 
	
	private static PublishSummaryPO publishSummaryPOInstance;
	
	private Product productToCheck;
	
	public static PublishSummaryPO getInstance(){
		getDriverInstance().manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		if(publishSummaryPOInstance == null){
			summaryTable = findDataTableInsideOf(findElement(withId("summaryForm:listingList")));
			publishSummaryPOInstance = new PublishSummaryPO();
		}
		return publishSummaryPOInstance;
	}

	public PublishSummaryPO check(Product productToCheck){
		this.productToCheck = productToCheck;
		return getInstance();
	}
	
	public PublishSummaryPO IsPublished(){
		if(!summaryTable.findRowWithText(this.productToCheck.getName()).
												getColumnAt(0).findElement(withTag("img")).
													getAttribute("id").contains("ok")){
			throw new TestFailedException("Anúncio "+this.productToCheck.getName()+" não foi publicado com sucesso na tela de sumário");
		}
		return getInstance();
	}

	public FourthStepPO publishSummary() {
		// TODO Auto-generated method stub
		return null;
	}
}
