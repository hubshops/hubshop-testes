package br.com.hubshop.suites.publish;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import br.com.hubshop.framework.BasePO;

public abstract class  AbstractStepByStepPO extends BasePO{
	
	@FindBy(how=How.ID,id="stepBack")
	private WebElement stepBackButton;	

//	private static FirstStepPO firstStepPO;
	
//	private static SecondStepPO secondStepPO;
	
	private static ThirdStepPO thirdStepPO;
	
	private static FourthStepPO fourthStepPO;
	
	public static FirstStepPO firstStep(){
		PageFactory.initElements(getDriverInstance(),FirstStepPO.class);
		return FirstStepPO.getInstance();
	}
	
	public SecondStepPO goToSecondStep(){
		navegateToSecondStep();
		PageFactory.initElements(getDriverInstance(),SecondStepPO.class);
		return SecondStepPO.getInstance();
	}
	
	public ThirdStepPO goToThirdStep(){
		navegateToThirdStep();
		return ThirdStepPO.getInstance();
	}
	
	public FourthStepPO goToFourthStep(){
		navegateToFourthStep();
		return FourthStepPO.getInstance();
	}
	
	public abstract int getCurrentStep();
	
	private SecondStepPO navegateToSecondStep(){
		switch (getCurrentStep()) {
		case 1:
			clickAndWaitLoadingMessageAt(findElement(withId("nextStep")));
			break;

		default:
			break;
		}
		return SecondStepPO.getInstance();
	}
	
	public ThirdStepPO navegateToThirdStep(){
		switch (getCurrentStep()) {
		case 2:
			clickAndWaitLoadingMessageAt(findElement(withId("nextStep")));
			break;

		default:
			break;
		}
		return ThirdStepPO.getInstance();
	}
	
	public FourthStepPO navegateToFourthStep(){
		switch (getCurrentStep()) {
		case 3:
			clickAndWaitLoadingMessageAt(findElement(withId("nextStep")));
			break;

		default:
			break;
		}
		return FourthStepPO.getInstance();
	}
}
