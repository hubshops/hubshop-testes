package br.com.hubshop.suites.publish;

import org.testng.annotations.Test;

import br.com.hubshop.base.listing.ListingModel;
import br.com.hubshop.base.product.Product;
import br.com.hubshop.mercadolivre.listing.MercadoLivreListingModel;
import br.com.hubshop.mercadolivre.product.category.MercadoLivreCategory;
import br.com.hubshop.suites.HubshopTest;
import br.com.hubshop.suites.mercadolivre.site.MercadoLivreListingAux;
import br.com.hubshop.suites.mercadolivre.site.MLProductPagePO;

public class PublishTest extends HubshopTest{
	
	
	@Test
	public void publishValidProduct(){
		
		Product product = new Product();
		product.setName("Abridor De Garrafa Que Toca Hino Do Botafogo");
		
		ListingModel model = new MercadoLivreListingModel();
		model.setName("Teste");
		
		PublishFlow.
			publish(product).
				goToFirstStep().chooseCategorySelectMode(CategorySelectModeEnum.ONE_FOR_ALL_PRODUCTS).
					goToSecondStep().searchCategoryByName("abridores").andChooseFirstOption().
						goToThirdStep().chooseModel(model).
							goToFourthStep().check(product).IsOkToPublish().publishListings().
								publishSummary().check(product).IsPublished();
//		
		MercadoLivreListingAux botafogoListing = new MercadoLivreListingAux();
		botafogoListing.setPriceOnPage("R$ 2290");
		botafogoListing.setName("Abridor De Garrafa Que Toca Hino Do Botafogo");
		botafogoListing.setCategory(new MercadoLivreCategory("Coleções e Comics Latas, Garrafas e Afins Cervejas Abridores"));
		botafogoListing.setListingDescription("teste");
		
		
		MLProductPagePO.gotoProductPage("http://produto.mercadolivre.com.br/MLB-709869502-abridor-de-garrafa-que-toca-hino-do-botafogo-_JM").
				and().checkIsEqualTo(botafogoListing);
		
	}



}
