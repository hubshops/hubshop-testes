package br.com.hubshop.suites.publish;

import br.com.hubshop.base.product.Product;
import br.com.hubshop.suites.productcatalog.ProductCatalogPO;

public class PublishFlow {
	
	private static PublishFlow publishFlowInstance;
	
//	private static AbstractStepByStepPO abstractStepByStepPO;
	
	private Product productToBePublished;
	
	public FirstStepPO goToFirstStep(){
		return AbstractStepByStepPO.firstStep();				
	}
	
	private static PublishFlow getInstance(){
		if(publishFlowInstance == null){
			publishFlowInstance = new PublishFlow();
		}
		return publishFlowInstance;
	}
	
	public static PublishFlow publish(Product product){
		ProductCatalogPO.goToPage();
		ProductCatalogPO.selectAndPublishProduct(product);
		getInstance().setProductToBePublished(product);
		return getInstance();
	}

	protected Product getProductToBePublished() {
		return productToBePublished;
	}

	protected void setProductToBePublished(Product productToBePublished) {
		this.productToBePublished = productToBePublished;
	}

}
