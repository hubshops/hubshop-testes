package br.com.hubshop.suites.publish;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SecondStepPO extends AbstractStepByStepPO{
	
	private static SecondStepPO secondStepPOInstance;	
	
	
	public static SecondStepPO getInstance(){
		if(secondStepPOInstance == null){
			secondStepPOInstance = new SecondStepPO();
		}
		return secondStepPOInstance;
	}
	
	@FindBy(how=How.ID,id="oneChoiceTabView:findCategory_input")
	private static WebElement categoryNameSearchInput;
	
	public SecondStepPO searchCategoryByName(String categoryName){
		typeText(categoryName, categoryNameSearchInput);
		waitForLoadingMessageDisapear();		
		return getInstance();
	}
	
	
	public SecondStepPO andChooseFirstOption(){
		clickAt(findElement(withXpath("//div[@id='oneChoiceTabView:findCategory_panel']/table/tbody/tr/td[2]")));
		return getInstance();
	}

	@Override
	public int getCurrentStep() {
		return 2;
	}

}
