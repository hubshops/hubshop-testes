package br.com.hubshop.suites.publish;

import br.com.hubshop.base.product.Product;
import br.com.hubshop.exception.TestFailedException;
import br.com.hubshop.framework.datatable.WebTable;

public class FourthStepPO extends AbstractStepByStepPO {

	private static FourthStepPO fourthStepPOInstance;
	private Product productToCheck;
	private static WebTable productReviewTable;
	
	
	public static FourthStepPO getInstance(){
		if(fourthStepPOInstance == null){
			fourthStepPOInstance = new FourthStepPO();
			productReviewTable = findDataTableInsideOf(findElement(withId("productReview")));
		}
		return fourthStepPOInstance;
	}
	
	public FourthStepPO check(Product productToCheck){
		this.productToCheck = productToCheck;
		return getInstance();
	}
	
	public FourthStepPO IsOkToPublish(){
		if(!productReviewTable.findRowWithText(productToCheck.getName()).
								getColumnAt(0).
									findElement(withTag("img")).
										getAttribute("id").
											contains("ok")){
			throw new TestFailedException("Produto "+productToCheck.getName()+" não está pronto para ser publicado");
		}
		return getInstance();
	}
	
	
	public PublishSummaryPO publishListings(){
		clickAndWaitLoadingMessageAt(findElement(withId("publishListings")));
		return PublishSummaryPO.getInstance();
	}
	
	@Override
	public int getCurrentStep() {
		return 4;
	}

}
