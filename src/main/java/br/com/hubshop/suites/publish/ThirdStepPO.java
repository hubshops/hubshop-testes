package br.com.hubshop.suites.publish;

import br.com.hubshop.base.listing.ListingModel;

public class ThirdStepPO extends AbstractStepByStepPO{

	private static ThirdStepPO thirdStepPOInstance;
	
	public static ThirdStepPO getInstance(){
		if(thirdStepPOInstance == null){
			thirdStepPOInstance = new ThirdStepPO();
		}
		return thirdStepPOInstance;
	}
	
	public ThirdStepPO chooseModel(ListingModel model){
		selectOptionByText("mllModel", model.getName());
		return getInstance();
	}

	@Override
	public int getCurrentStep() {
		return 3;
	}
	
}
