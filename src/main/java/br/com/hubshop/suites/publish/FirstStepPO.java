package br.com.hubshop.suites.publish;

public class FirstStepPO extends AbstractStepByStepPO{
	
	private static FirstStepPO firstStepPOInstance;
	
	public static FirstStepPO getInstance(){
		if(firstStepPOInstance == null){
			firstStepPOInstance = new FirstStepPO();
		}
		return firstStepPOInstance;
	}
	
	public FirstStepPO chooseCategorySelectMode(CategorySelectModeEnum categorySelectMode){		
		switch (categorySelectMode) {
			case ONE_FOR_ALL_PRODUCTS:
				clickAt(findElement(withId("categoryChoiceMode:0")));
				break;
			case SUGGESTED:	
				clickAt(findElement(withId("categoryChoiceMode:1")));				
				break;
		}
		return getInstance();
	}


	@Override
	public int getCurrentStep() {
		return 1;
	}
	

}
