package br.com.hubshop.domain;

public enum ChannelEnum {

	MAGENTO("Magento"), MERCADO_LIVRE("Mercado Livre");
	
	private String label;
	
	private ChannelEnum(String label){
		this.label = label;
	}
	
	public String getLabel(){
		return label;
	}
	
}
