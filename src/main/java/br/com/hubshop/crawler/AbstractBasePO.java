package br.com.hubshop.crawler;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public abstract class AbstractBasePO {

	private static WebDriver driver  = null;
	
	protected void goToUrl(String url){
		getDriverInstance().navigate().to(url);
	}
	
	protected WebDriver getDriverInstance() {
		if (driver == null) {
			initDriverChrome();
		}
		return driver;
	}
	
	private WebDriver initDriverChrome() {
		System.setProperty("webdriver.chrome.driver",
				"D:/Empresa/HubShop/Dev/workspaces/workspace-hubshop-dev/framework-testes/src/main/resources/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return driver;
	}
	
	protected WebElement findElement(By by){
		return getDriverInstance().findElement(by);
	}
	
	protected List<WebElement> findMultipleElements(By by){
		return getDriverInstance().findElements(by);
	}
}
