package br.com.hubshop.crawler;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class MercadoLivreProfilePO extends AbstractBasePO {
	
	private WebElement sells;
	
	public static MercadoLivreProfilePO gotoPage(String productUrl) {
		MercadoLivreProfilePO mercadoLivreProfilePO = new MercadoLivreProfilePO();
		mercadoLivreProfilePO.goToUrl(productUrl);
		return mercadoLivreProfilePO.init();
	}
	
	private MercadoLivreProfilePO init(){
		List<WebElement> pageSells = findMultipleElements(By.cssSelector("dd.reputation-relevant strong"));
		if (pageSells.size() >= 2) {
			sells = pageSells.get(1);
		} else {
			sells = findElement(By.cssSelector("nav.reputation-filters a b"));
		}
		
		return this;
	}
	
	public String getNumberOfSells() {
		return sells.getText();
	}
	
}