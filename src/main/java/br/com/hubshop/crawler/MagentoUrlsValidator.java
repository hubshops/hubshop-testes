package br.com.hubshop.crawler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.NoSuchWindowException;
import org.testng.annotations.Test;

import br.com.hubshop.framework.BaseTest;

public class MagentoUrlsValidator extends BaseTest {
	
	@Test
	public void searchMagentoStoresAndMercadoLivreProfiles() {
		List<String> urls = readUrlsFromFile("D:\\Temp\\links1.txt");
		
		System.out.println(format("dominio", "magento", "ja_vende_ml", "qnt_vendas_ml", "perfil_ml"));
		for (String url: urls) {
			try {
				System.out.print(verifyIfIsAMagentoStoreAndHasMercadoLivreProfile(url));
			} catch (NoSuchWindowException e) {
				System.out.println("Fechou a janela. Saindo dos testes...");
				break;
			} catch (Throwable e) {
				System.out.println(format(url, "Erro: " + e));
			}
		}
	}
	
	private String verifyIfIsAMagentoStoreAndHasMercadoLivreProfile(String url) {
		String hasMagentoAdminEndpoint = hasMagentoAdminEndpoint(url) ? "sim" : "talvez/nao tem admin";
		String linkToProfileMercadoLivre = getLinkToProfileMercadoLivre(url);
		if (linkToProfileMercadoLivre != null) {
			String numberOfSells = getProfileNumberOfSells(linkToProfileMercadoLivre);
			return format(url, hasMagentoAdminEndpoint, "sim", numberOfSells, linkToProfileMercadoLivre);
		} else {
			return format(url, hasMagentoAdminEndpoint, "nao", "-", "-");
		}
	}
	
	private String getLinkToProfileMercadoLivre(String url) {
		String siteName = url.split("\\.")[0];
		GoogleSearchPO googleSearch = GoogleSearchPO.query(siteName);
		
		if (googleSearch.getFirstResultText().contains("MercadoLivre")) {
			return googleSearch.getFirstResultLink();
		}
		return null;
	}

	private boolean hasMagentoAdminEndpoint(String url) {
		String adminPageTitle = MagentoAdminPO.gotoPage("http://" + url + "/admin").getTitle();
		return adminPageTitle.toLowerCase().contains("magento");
	}
	
	private String getProfileNumberOfSells(String url) {
		MercadoLivreProfilePO mercadoLivreProfile = MercadoLivreProfilePO.gotoPage(url);
		return mercadoLivreProfile.getNumberOfSells();
	}
	
	private List<String> readUrlsFromFile(String fileName) {
		List<String> urls = new ArrayList<>();
		BufferedReader br = null;
		try {
			String sCurrentLine;
			br = new BufferedReader(new FileReader(fileName));
			while ((sCurrentLine = br.readLine()) != null) {
				urls.add(sCurrentLine);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return urls;
	}
	
	private String format(String ... args) {
		return String.join(",", args) + "\n";		
	}

}
