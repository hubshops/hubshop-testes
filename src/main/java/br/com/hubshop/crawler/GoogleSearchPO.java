package br.com.hubshop.crawler;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class GoogleSearchPO extends AbstractBasePO {
	
	private WebElement firstResult;
	
	public static GoogleSearchPO gotoPage(String productUrl) {
		GoogleSearchPO googleSearchPO = new GoogleSearchPO();
		googleSearchPO.goToUrl(productUrl);
		return googleSearchPO.init();
	}
	
	public static GoogleSearchPO query(String queryString) {
		return gotoPage("https://www.google.com.br/?q=" + queryString + "+perfil+do+vendedor&gws_rd=ssl#safe=off&q=" +queryString + "+perfil+do+vendedor");
	}
	
	private GoogleSearchPO init(){
		firstResult = findElement(By.cssSelector("h3.r a"));
		return this;
	}
	
	public String getFirstResultText() {
		return firstResult.getText();
	}
	
	public String getFirstResultLink() {
		return firstResult.getAttribute("href");
	}

}