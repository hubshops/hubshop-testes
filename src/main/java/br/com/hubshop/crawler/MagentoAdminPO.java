package br.com.hubshop.crawler;

public class MagentoAdminPO extends AbstractBasePO {
	
	private String title;
	
	public static MagentoAdminPO gotoPage(String productUrl) {
		MagentoAdminPO magentoAdminPO = new MagentoAdminPO();
		magentoAdminPO.goToUrl(productUrl);		
		return magentoAdminPO.init();
	}
	
	private MagentoAdminPO init(){
		title = getDriverInstance().getTitle();
		return this;
	}

	public String getTitle() {
		return title;
	}
}
